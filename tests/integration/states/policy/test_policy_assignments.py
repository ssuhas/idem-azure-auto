import copy
import time
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_policy_assignment(
    hub, ctx, resource_group_fixture, policy_definition_fixture
):
    """
    This test provisions a policy assignment, describes policy assignment and deletes the assigned policy.
    """
    # Create policy assignment
    policy_assignment_name = "idem-test-location-policy-assignment-" + str(
        int(time.time())
    )
    resource_group_name = resource_group_fixture.get("name")
    policy_definition_id = policy_definition_fixture.get("id")
    scope = f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
    policy_assign_parameters = {
        "parameters": {"allowedLocations": {"value": ["southindia", "eastus"]}}
    }

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    # Create policy assignment with --test
    policy_assign_ret = await hub.states.azure.policy.policy_assignments.present(
        test_ctx,
        name=policy_assignment_name,
        policy_assignment_name=policy_assignment_name,
        policy_definition_id=policy_definition_id,
        scope=scope,
        **policy_assign_parameters,
    )
    assert policy_assign_ret["result"], policy_assign_ret["comment"]
    assert not policy_assign_ret["old_state"] and policy_assign_ret["new_state"]
    assert (
        f"Would create azure.policy.policy_assignments '{policy_assignment_name}'"
        in policy_assign_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=policy_assign_ret["new_state"],
        expected_old_state=None,
        expected_new_state=policy_assign_parameters,
        policy_assignment_name=policy_assignment_name,
        policy_definition_id=policy_definition_id,
        scope=scope,
        idem_resource_name=policy_assignment_name,
    )
    resource_id = policy_assign_ret["new_state"].get("resource_id")
    assert (
        f"{scope}/providers/Microsoft.Authorization/policyAssignments/{policy_assignment_name}"
        == resource_id
    )

    # Create policy assignment in real
    policy_assign_ret = await hub.states.azure.policy.policy_assignments.present(
        ctx,
        name=policy_assignment_name,
        policy_assignment_name=policy_assignment_name,
        policy_definition_id=policy_definition_id,
        scope=scope,
        **policy_assign_parameters,
    )
    assert policy_assign_ret["result"], policy_assign_ret["comment"]
    assert not policy_assign_ret["old_state"] and policy_assign_ret["new_state"]
    check_returned_states(
        old_state=None,
        new_state=policy_assign_ret["new_state"],
        expected_old_state=None,
        expected_new_state=policy_assign_parameters,
        policy_assignment_name=policy_assignment_name,
        policy_definition_id=policy_definition_id,
        scope=scope,
        idem_resource_name=policy_assignment_name,
    )
    resource_id = policy_assign_ret["new_state"].get("resource_id")
    assert (
        f"{scope}/providers/Microsoft.Authorization/policyAssignments/{policy_assignment_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"https://management.azure.com{resource_id}?api-version=2021-06-01",
        retry_count=5,
        retry_period=10,
    )

    # Describe policy assignment
    describe_ret = await hub.states.azure.policy.policy_assignments.describe(ctx)
    assert resource_id in describe_ret
    describe_resource = describe_ret.get(resource_id)
    described_resource = describe_resource.get(
        "azure.policy.policy_assignments.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=described_resource_map,
        policy_assignment_name=policy_assignment_name,
        policy_definition_id=policy_definition_id,
        scope=scope,
        idem_resource_name=resource_id,
    )

    pd_update_parameters = {"parameters": {"allowedLocations": {"value": ["eastus"]}}}
    # Update policy definition with --test
    policy_assign_ret = await hub.states.azure.policy.policy_assignments.present(
        test_ctx,
        name=policy_assignment_name,
        policy_assignment_name=policy_assignment_name,
        policy_definition_id=policy_definition_id,
        scope=scope,
        **pd_update_parameters,
    )
    assert policy_assign_ret["result"], policy_assign_ret["comment"]
    assert policy_assign_ret["old_state"] and policy_assign_ret["new_state"]
    assert (
        f"Would update azure.policy.policy_assignments '{policy_assignment_name}'"
        in policy_assign_ret["comment"]
    )
    check_returned_states(
        old_state=policy_assign_ret["old_state"],
        new_state=policy_assign_ret["new_state"],
        expected_old_state=policy_assign_parameters,
        expected_new_state=pd_update_parameters,
        policy_assignment_name=policy_assignment_name,
        policy_definition_id=policy_definition_id,
        scope=scope,
        idem_resource_name=policy_assignment_name,
    )
    resource_id = policy_assign_ret["new_state"].get("resource_id")
    assert (
        f"{scope}/providers/Microsoft.Authorization/policyAssignments/{policy_assignment_name}"
        == resource_id
    )

    # Update policy definition in real
    policy_assign_ret = await hub.states.azure.policy.policy_assignments.present(
        ctx,
        name=policy_assignment_name,
        policy_assignment_name=policy_assignment_name,
        policy_definition_id=policy_definition_id,
        scope=scope,
        **pd_update_parameters,
    )
    assert policy_assign_ret["result"], policy_assign_ret["comment"]
    assert policy_assign_ret["old_state"] and policy_assign_ret["new_state"]
    assert (
        f"Updated azure.policy.policy_assignments '{policy_assignment_name}'"
        in policy_assign_ret["comment"]
    )
    check_returned_states(
        old_state=policy_assign_ret["old_state"],
        new_state=policy_assign_ret["new_state"],
        expected_old_state=policy_assign_parameters,
        expected_new_state=pd_update_parameters,
        policy_assignment_name=policy_assignment_name,
        policy_definition_id=policy_definition_id,
        scope=scope,
        idem_resource_name=policy_assignment_name,
    )
    resource_id = policy_assign_ret["new_state"].get("resource_id")
    assert (
        f"{scope}/providers/Microsoft.Authorization/policyAssignments/{policy_assignment_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"https://management.azure.com{resource_id}?api-version=2021-06-01",
        retry_count=5,
        retry_period=10,
    )

    # Delete policy definition with --test
    pd_del_ret = await hub.states.azure.policy.policy_assignments.absent(
        test_ctx,
        name=policy_assignment_name,
        policy_assignment_name=policy_assignment_name,
        scope=scope,
    )
    assert pd_del_ret["result"], pd_del_ret["comment"]
    assert pd_del_ret["old_state"] and not pd_del_ret["new_state"]
    assert (
        f"Would delete azure.policy.policy_assignments '{policy_assignment_name}'"
        in pd_del_ret["comment"]
    )
    check_returned_states(
        old_state=pd_del_ret["old_state"],
        new_state=None,
        expected_old_state=pd_update_parameters,
        expected_new_state=None,
        policy_assignment_name=policy_assignment_name,
        policy_definition_id=policy_definition_id,
        scope=scope,
        idem_resource_name=policy_assignment_name,
    )

    # Delete policy definitions
    pd_del_ret = await hub.states.azure.policy.policy_assignments.absent(
        ctx,
        name=policy_assignment_name,
        policy_assignment_name=policy_assignment_name,
        scope=scope,
    )
    assert pd_del_ret["result"], pd_del_ret["comment"]
    assert pd_del_ret["old_state"] and not pd_del_ret["new_state"]
    assert (
        f"Deleted azure.policy.policy_assignments '{policy_assignment_name}'"
        in pd_del_ret["comment"]
    )
    check_returned_states(
        old_state=pd_del_ret["old_state"],
        new_state=None,
        expected_old_state=pd_update_parameters,
        expected_new_state=None,
        policy_assignment_name=policy_assignment_name,
        policy_definition_id=policy_definition_id,
        scope=scope,
        idem_resource_name=policy_assignment_name,
    )

    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"https://management.azure.com{resource_id}?api-version=2021-06-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete policy definition again
    pd_del_ret = await hub.states.azure.policy.policy_assignments.absent(
        ctx,
        name=policy_assignment_name,
        policy_assignment_name=policy_assignment_name,
        scope=scope,
    )
    assert pd_del_ret["result"], pd_del_ret["comment"]
    assert not pd_del_ret["old_state"] and not pd_del_ret["new_state"]
    assert (
        f"azure.policy.policy_assignments '{policy_assignment_name}' already absent"
        in pd_del_ret["comment"]
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    policy_assignment_name,
    policy_definition_id,
    scope,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert policy_assignment_name == old_state.get("policy_assignment_name")
        assert policy_definition_id == old_state.get("policy_definition_id")
        assert scope == old_state.get("scope")
        assert expected_old_state["parameters"] == old_state.get("parameters")
    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert policy_assignment_name == new_state.get("policy_assignment_name")
        assert policy_definition_id == new_state.get("policy_definition_id")
        assert scope == new_state.get("scope")
        assert expected_new_state["parameters"] == new_state.get("parameters")
